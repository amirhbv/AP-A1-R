#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>
#include <vector>
#include <cstdio>
using namespace std;
struct Contact{
    int id;
    string firstName;
    string lastName;
    string email;
    string phoneNumber;
    string address;
};
ostream& operator<<(ostream& outputStream, const Contact& temp)
{
    outputStream << temp.id << ' ';
    outputStream << temp.firstName << ' ';
    outputStream << temp.lastName << ' ';
    outputStream << temp.email << ' ';
    outputStream << temp.phoneNumber;
    if(!temp.address.empty())
        outputStream << ' ' << temp.address;
    outputStream << '\n';
    return outputStream;
}
bool addContact(vector<Contact>&, Contact);
bool updateContact(vector<Contact>&, Contact);
bool deleteContact(vector<Contact>&, int);
Contact getContact(string);
bool isContactOk(vector<Contact>&, Contact);
int findIndexById(vector<Contact>&, int);
int findMaxId(vector<Contact>&);
bool isPhoneNumberOk(string);
bool isEmailOk(string);
void searchByKeyword(vector<Contact>&, string);
void printContact(Contact);
void printAll(vector<Contact>&);
vector<Contact> readFromFile();
void wrtieToFile(vector<Contact>&);
void test();
void testGetContact();
void testIsPhoneNumberOk();
void testIsEmailOk();
void testIsContactOk();
void testFindIndexById();
void testFindMaxId();

#define OK "Command Ok\n"
#define FAILED "Command Failed\n"
#define PHONENUMBERLENGTH 11
#define FILENAME "contacts.csv"
#define NOTFOUND -1
#define NEWCONTACTDEFAULTID -1
//#define Debug
int main()
{
    test();
    #ifndef Debug
        freopen("/dev/null", "w", stderr);
    #endif // Debug
    vector<Contact> ContactDun;

    ContactDun = readFromFile();

    string line, command;
    while(getline(cin, line))
    {
    	printAll(ContactDun);
    	stringstream inputStream;
        inputStream << line;;
        inputStream >> command;
        if(command == "add")
        {
            if(addContact(ContactDun, getContact(line)))
                cout << OK;
            else
                cout << FAILED;
        }
        else if(command == "update")
        {
            if(updateContact(ContactDun, getContact(line)))
                cout << OK;
            else
                cout << FAILED;
        }
        else if(command == "delete")
        {
            int id;
            inputStream >> id;
            if(deleteContact(ContactDun, id))
                cout << OK;
            else
                cout << FAILED;
        }
        else if(command == "search")
        {
        	string keyword;
        	inputStream >> keyword;
        	searchByKeyword(ContactDun, keyword);
        }
        else
            cout << FAILED;
    }

    wrtieToFile(ContactDun);
    return 0;
}

Contact getContact(string inputLine)
{
	stringstream inputStream(inputLine);
    string temp;
    inputStream >> temp;
    Contact newContact;
    newContact.id = NEWCONTACTDEFAULTID;
    if(temp == "update")
    	inputStream >> newContact.id;
    while(inputStream >> temp)
    {
    	if(temp == "-f")
    		inputStream >> newContact.firstName;
    	else if(temp == "-l")
    		inputStream >> newContact.lastName;
    	else if(temp == "-p")
    		inputStream >> newContact.phoneNumber;
    	else if(temp == "-e")
    		inputStream >> newContact.email;
        else if(temp == "-a")
        {
            char temp;
            inputStream.get(temp);
            while(inputStream.get(temp))
            {
                if(temp == '-')
                {
                    inputStream << '-';
                    break;
                }
                newContact.address += temp;
            }
        }
    }
    return newContact;
}

bool isPhoneNumberOk(string phoneNumber)
{
	if(phoneNumber[0] != '0' || phoneNumber[1] != '9')
		return 0;
	if(phoneNumber.size() != PHONENUMBERLENGTH)
		return 0;
	for(unsigned int i = 0; i < phoneNumber.size(); i++)
		if(phoneNumber[i] < '0' || phoneNumber[i] > '9')
			return 0;
	return 1;
}

bool isEmailOk(string email)
{
	int dotFlag = 0, atCounter = 0;
	for(unsigned int i = 0; i < email.size() ; i++)
	{
		if(email[i] == ',')
			return 0;
		if(email[i] == '@')
			atCounter++;
		if(atCounter && email[i] == '.' && email[i-1] != '@')
			dotFlag = 1;
	}
	return dotFlag && (atCounter==1);

}

bool isContactOk(vector<Contact>& ContactDun, Contact newContact)
{
	if(!isPhoneNumberOk(newContact.phoneNumber) || !isEmailOk(newContact.email))
		return 0;
	for(unsigned int i = 0 ; i < ContactDun.size() ; i++)
	{
		if( (ContactDun[i].phoneNumber == newContact.phoneNumber || ContactDun[i].email == newContact.email)
						&& newContact.id != ContactDun[i].id )
			return 0;
		if(ContactDun[i].firstName == newContact.firstName && ContactDun[i].lastName == newContact.lastName
						&& newContact.id != ContactDun[i].id )
			return 0;
	}
	if(newContact.id == NEWCONTACTDEFAULTID)
		if(newContact.firstName.empty() || newContact.lastName.empty() ||
			newContact.phoneNumber.empty() || newContact.email.empty() )
				return 0;
	return 1;
}

int findIndexById(vector<Contact>& ContactDun, int id)
{
	for(unsigned int i = 0 ; i < ContactDun.size() ; i++)
		if(ContactDun[i].id == id)
			return i;
	return NOTFOUND;
}

int findMaxId(vector<Contact>& ContactDun)
{
	int result = -1;
	for(unsigned int i = 0 ; i < ContactDun.size() ; i++)
		if(ContactDun[i].id > result)
			result = ContactDun[i].id;
	return result;
}


bool addContact(vector<Contact>& ContactDun, Contact newContact)
{
	if(isContactOk(ContactDun, newContact))
	{
		newContact.id = findMaxId(ContactDun) + 1;
		ContactDun.push_back(newContact);
		return 1;
	}
	return 0;
}

bool updateContact(vector<Contact>& ContactDun, Contact newContact)
{
	if(isContactOk(ContactDun, newContact))
	{
		int index = findIndexById(ContactDun, newContact.id);
		if(index == NOTFOUND)
            return 0;

		if( newContact.firstName.empty() )
			newContact.firstName = ContactDun[index].firstName;

		if( newContact.lastName.empty() )
			newContact.lastName = ContactDun[index].lastName;

		if( newContact.phoneNumber.empty() )
			newContact.phoneNumber = ContactDun[index].phoneNumber;

		if( newContact.email.empty() )
			newContact.email = ContactDun[index].email;

        if( newContact.address.empty() )
			newContact.address = ContactDun[index].address;

		ContactDun.erase(ContactDun.begin() + index);
		ContactDun.push_back(newContact);
		return 1;
	}
	return 0;
}

bool deleteContact(vector<Contact>& ContactDun, int id)
{
    int index = findIndexById(ContactDun, id);
    if(index == NOTFOUND)
        return 0;

    ContactDun.erase(ContactDun.begin() + index);
    return 1;
}

void searchByKeyword(vector<Contact>& ContactDun, string keyword)
{
	for(unsigned int i = 0; i < ContactDun.size() ; i++)
	{
		if(ContactDun[i].firstName.find(keyword) != string::npos ||
			ContactDun[i].lastName.find(keyword) != string::npos ||
			ContactDun[i].phoneNumber.find(keyword) != string::npos ||
			ContactDun[i].email.find(keyword) != string::npos ||
            ContactDun[i].address.find(keyword) != string::npos )

                printContact(ContactDun[i]);
	}
}

void printContact(Contact contact)
{
	cout << contact;
}

void printAll(vector<Contact>& ContactDun)
{
	for(unsigned int i = 0; i< ContactDun.size(); i++)
		cerr << ContactDun[i];
	cerr << "\n\n";
}

vector<Contact> readFromFile()
{
    vector<Contact> ContactDun;
	ifstream fin;
	fin.open(FILENAME);
	string format;
	getline(fin, format);
	Contact temp;
	while(fin >> temp.id)
	{
		fin >> temp.firstName >> temp.lastName >> temp.email >> temp.phoneNumber;
		char delim;
		fin.get(delim);
		if(delim == ' ')
            getline(fin, temp.address);
		ContactDun.push_back(temp);
	}
	fin.close();
	return ContactDun;
}

void wrtieToFile(vector<Contact>& ContactDun)
{
	ofstream fout;
	fout.open(FILENAME, ofstream::out | ofstream::trunc);
	fout << "<id> <first_name> <last_name> <email_address> <phone_number> [address]\n";
	for(unsigned int i = 0 ; i < ContactDun.size() ; i++)
        fout << ContactDun[i];
	fout.close();
}

void test()
{
    testGetContact();
    testIsPhoneNumberOk();
    testIsEmailOk();
    testIsContactOk();
    testFindIndexById();
    testFindMaxId();
}

void testGetContact()
{
    string s1 = "add -f amir -l habib -p 09367552672 -e amir@gmail.com";
    string s2 = "add -l habib -f karim -p 09367552673 -e amin@gmail.com";
    string s3 = "update 1 -f amir -l habib -e amir@gmail.com -p 09367552674";
    string s4 = "add -p 09367552675       -f amir -l habib -e amiri@gmail.com";
    Contact testContact = getContact(s1);
    assert(testContact.firstName == "amir");
    assert(testContact.lastName == "habib");
    testContact = getContact(s2);
    assert(testContact.lastName == "habib");
    testContact = getContact(s3);
    assert(testContact.email == "amir@gmail.com");
    testContact = getContact(s4);
    assert(testContact.email == "amiri@gmail.com");
    assert(testContact.phoneNumber == "09367552675");
    assert(testContact.firstName == "amir");
    cout << "Get Contact is OK!" << endl;
}

void testIsPhoneNumberOk()
{
    assert(isPhoneNumberOk("9367552672") == 0);
    assert(isPhoneNumberOk("19367552672") == 0);
    assert(isPhoneNumberOk("093a7552672") == 0);
    assert(isPhoneNumberOk("09367552672") == 1);
    cout << "Is PhoneNumber OK is OK!" << endl;
}

void testIsEmailOk()
{
    assert(isEmailOk("amir@gmail") == 0);
    assert(isEmailOk("amir@.gmail") == 0);
    assert(isEmailOk("am@ir@gmail.com") == 0);
    assert(isEmailOk("am,ir@gmail.com") == 0);
    assert(isEmailOk("amir@gmail.com") == 1);
    cout << "Is Email OK is OK!" << endl;
}

void testIsContactOk()
{
    vector<Contact> testVector;
    string s1 = "add -f amir -l habib -p 09367552672 -e amir@gmail.com";
    string s2 = "add -l habib -f karim -p 09367552673 -e amin@gmail.com";
    string s3 = "add -f amir -l habib -e amir@gmail.com -p 0936755267";
    Contact testContact = getContact(s1);
    assert(isContactOk(testVector, testContact) == 1);
    addContact(testVector, testContact);
    assert(isContactOk(testVector, testContact) == 0);
    testContact = getContact(s2);
    assert(isContactOk(testVector, testContact) == 1);
    addContact(testVector, testContact);
    assert(isContactOk(testVector, testContact) == 0);
    testContact = getContact(s3);
    assert(isContactOk(testVector, testContact) == 0);
    cout << "Is Contact OK is OK!" << endl;
}

void testFindIndexById()
{
    vector<Contact> testVector;
    Contact testContact;
    testContact.id = 0;testVector.push_back(testContact);
    testContact.id = 4;testVector.push_back(testContact);
    testContact.id = 10;testVector.push_back(testContact);
    testContact.id = 7;testVector.push_back(testContact);
    assert(findIndexById(testVector, 0) == 0);
    assert(findIndexById(testVector, 4) == 1);
    assert(findIndexById(testVector, 10) == 2);
    assert(findIndexById(testVector, 7) == 3);
    assert(findIndexById(testVector, 2) == NOTFOUND);
    assert(findIndexById(testVector, -2) == NOTFOUND);
    cout << "Find Index by ID is OK!" << endl;
}

void testFindMaxId()
{
    vector<Contact> testVector;
    Contact testContact;
    testContact.id = 0;testVector.push_back(testContact);
    assert(findMaxId(testVector) == 0);
    testContact.id = 4;testVector.push_back(testContact);
    assert(findMaxId(testVector) == 4);
    testContact.id = 10;testVector.push_back(testContact);
    assert(findMaxId(testVector) == 10);
    testContact.id = 7;testVector.push_back(testContact);
    assert(findMaxId(testVector) == 10);
    cout << "Find Max ID is OK!" << endl;
}
