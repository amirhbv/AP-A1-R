# Contact Dun

## UT AP Spring 97


terminal based cpp app for managing contacts
you can easily add or remove contacts from your phonebook
or update them,
contacts must have first name and last name and phone number and an email address and can have address,
you can search between them by any of these properties.

### commands 
    aruguments can be in any order
```
add <-f first_name> <-l last_name> <-p phone_number> <-e email_address> [-a address]
update <id> [-f first_name] [-l last_name] [-p phone_number] [-e email_address] [-a address]
delete <id>

output form : <Command Ok | Command Failed>

search <keyword>

output form : <id> <first_name> <last_name> <email_address> <phone_number> [address]
```

phone number must be 11 digits starting with 09 and email must have a @ before a . character.

contacts will be saved in contacts.csv file in csv format and can will be used in later in the app.

### compile and run with g++
```
    g++ main.cpp
    ./a.out
```
tests run every time you run the app




